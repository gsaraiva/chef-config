# See https://docs.chef.io/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "extremez3r0"
client_key               "#{current_dir}/extremez3r0.pem"
validation_client_name   "99beds-validator"
validation_key           "#{current_dir}/99beds-validator.pem"
chef_server_url          "https://api.opscode.com/organizations/99beds"
cookbook_path            ["#{current_dir}/../cookbooks"]
